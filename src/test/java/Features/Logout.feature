Feature: LogoutFeature
  This feature deals with user logout functionality

  Scenario: Login with correct credentials and then logout to the main page
    Given I navigate to the login page
    And I enter the username as "opensourcecms" and password as "opensourcecms"
    And I click login button
    When I click logout button
    Then I should be on the main page