Feature: LoginFeature
  This feature deals with login functionality. Check if login is possible,
  and if credentials are set wrongly, it should rise an error

  Background: Go to specified page in order to log in
    Given I navigate to the login page

  Scenario: Login with correct credentials
    And I enter the username as "opensourcecms" and password as "opensourcecms"
    When I click login button
    Then I should see the dashboard page

  Scenario: Login with incorrect credentials
    And I enter the username as "opensourcecmsA" and password as "opensourcecmsA"
    When I click login button
    Then I should get an error


