package Pages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

@Getter
public class DashboardPage {

    public DashboardPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "companyLogoHeader")
    private WebElement logo;

    @FindBy(how = How.LINK_TEXT, using = "Logout")
    private WebElement logoutBtn;

    @FindBy(how = How.ID, using = "pim")
    private WebElement pimSubMenu;

    public boolean isUserLogged() {
        return logo.isDisplayed();
    }

    public void logoutUser() {
        logoutBtn.click();
    }

    public void goToPIM() {
        pimSubMenu.click();
    }
}