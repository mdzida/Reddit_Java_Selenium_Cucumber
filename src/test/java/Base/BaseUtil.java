package Base;

import Pages.LoginPage;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

@Getter
public class BaseUtil {
    private static final String ORANGE_HRM_ADDRESS = "https://s2.demo.opensourcecms.com/orangehrm/symfony/web/index.php/auth/login";
    private static final String LOGIN_AS_USERNAME = "opensourcecms";
    private static final String LOGIN_AS_PASSWORD = "opensourcecms";

    private WebDriver driver;
    private LoginPage loginPage;

    public void initializeDriver() {
        this.driver = new ChromeDriver();
    }

    public void openMainPage() {
        this.driver.get(ORANGE_HRM_ADDRESS);
    }

    public void closeWebDriver() {
        this.driver.quit();
    }

    public WebDriverWait waitForPageLoad() {
        return new WebDriverWait(driver, 2);
    }

    public void loginAsAdmin() {
        openMainPage();
        loginPage = new LoginPage(this.driver);
        loginPage.enterCredentials(LOGIN_AS_USERNAME, LOGIN_AS_PASSWORD);
        loginPage.clickLoginBtn();
    }
}